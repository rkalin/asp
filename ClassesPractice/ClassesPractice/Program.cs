﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Remoting.Messaging;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace PdfToDocxConverter
{
     
    class Program
    {
        static void Main(string[] args)
        {
            string pdfFile = @"C:\Users\Roman\Downloads\KP-KPI_PRESSURE_RUS_CATALOGUE.pdf";
            string wordFile = @"C:\Users\Roman\Downloads\temp.docx";

            SautinSoft.PdfFocus focus = new SautinSoft.PdfFocus();

            focus.OpenPdf(pdfFile);

            if (focus.PageCount > 0)
            {
                focus.WordOptions.Format = SautinSoft.PdfFocus.CWordOptions.eWordDocument.Docx;
                int result = focus.ToWord(wordFile);

                if(result == 0)
                {
                    System.Diagnostics.Process.Start(wordFile);
                }
            }

            Console.ReadKey();
           
        }
    }
}
